const checkboxes = document.querySelectorAll('#gestion-formes input');
checkboxes.forEach(checkbox => {
    checkbox.checked=true;
});


function updateText() {
    const text = document.getElementById('text-input').value;
    document.getElementById('text-large').textContent = text.toUpperCase();
    document.getElementById('text-medium').textContent = text;
    document.getElementById('text-script').textContent = text;
}

function updateTextSize() {
    const size = document.getElementById('text-size').value;
    document.getElementById('text-large').style.fontSize = size + 'px';
    document.getElementById('text-medium').style.fontSize = size + 'px';
    document.getElementById('text-script').style.fontSize = size + 'px';
}

function updateOrientation() {
    const orientation = document.querySelector('input[name="orientation"]:checked').value;
    const cardPreview = document.getElementById('card-preview');
    if (orientation === 'landscape') {
        cardPreview.style.width = '423px';
        cardPreview.style.height = '300px';
    } else {
        cardPreview.style.width = '300px';
        cardPreview.style.height = '423px';
    }
}

function updateBorder() {
    const border = document.querySelector('input[name="border"]:checked').value;
    const cardPreview = document.getElementById('card-preview');
    if (border === 'none') {
        cardPreview.style.border = 'none';
    } else if (border === 'auto') {
        cardPreview.style.border = '6px solid #000';
    } else {
        const color = document.getElementById('border-color').value;
        cardPreview.style.border = `6px solid ${color}`;
    }
    document.getElementById('border-color').disabled = border !== 'color';
}

function updateBorderColor() {
    const color = document.getElementById('border-color').value;
    document.getElementById('card-preview').style.border = `6px solid ${color}`;
}

function updateTextColor() {
    const color = document.getElementById('text-color').value;
    document.getElementById('card-text').style.color = color;
}

function uploadImage() {
    const input = document.getElementById('image-upload');
    const cardImage = document.getElementById('card-image');
    const cardPlaceholder = document.getElementById('card-placeholder');
    const reader = new FileReader();
    reader.onload = function(e) {
        cardImage.src = e.target.result;
        cardImage.style.display = 'block';
        cardPlaceholder.style.display = 'none';
    }
    reader.readAsDataURL(input.files[0]);
}

function generateCard() {
    const cardPreview = document.getElementById('card-preview');
    html2canvas(cardPreview).then(canvas => {
        const link = document.createElement('a');
        link.download = 'carte.png';
        link.href = canvas.toDataURL();
        link.click();
    });
}